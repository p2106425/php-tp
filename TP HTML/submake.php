<?php

session_start();

try {
    $iden = htmlentities($_POST['email']);
    $mdp = htmlentities($_POST['pass']);
    $nom = htmlentities($_POST['nom']);
    $prenom = htmlentities($_POST['prenom']);
    $date = htmlentities($_POST['date']);
    $adresse = htmlentities($_POST['adresse']);
    $ville = htmlentities($_POST['ville']);
    $code = htmlentities($_POST['code']);


    $pdo = new PDO('sqlite:database.db');
    $pdo->setAttribute(PDO::ATTR_ERRMODE , PDO::ERRMODE_EXCEPTION );
    $stmt = $pdo->prepare("insert into users values(? ,? , ? , ? ,?, ? , ? , ? , 1 ,datetime('now'));");
    $stmt->execute([null ,$iden,  $nom, $prenom,password_hash($mdp,PASSWORD_DEFAULT), $ville, $code,  $adresse ]);
    header('Location: Login.php');
}
catch(PDOException $e) {
    echo $e->getMessage();
}

