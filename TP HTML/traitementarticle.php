<?php
session_start();


if (isset($_POST['titre']) && isset($_POST['desc']) && isset($_POST['article'])) {
    $titre = htmlentities($_POST['titre']);
    $desc = htmlentities($_POST['desc']);
    $article = htmlentities($_POST['article']);
    $pdo = new PDO('sqlite:database.db');
    
         
        $pdo->exec('SET NAMES utf8');

        $pdo->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
        
    $stmt = $pdo->prepare("INSERT INTO article (Nom,Descr,Article) VALUES(?,?,?);");

    $stmt->execute([$titre, $desc, $article]);
    header('Location: index.php');
    exit();
}