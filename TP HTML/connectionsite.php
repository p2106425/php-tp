<?php 
session_start();

if (isset($_POST['log']) && isset($_POST['pass'])) {
    $iden = htmlentities($_POST['log']);
    $mdp = htmlentities($_POST['pass']);
    $pdo = new PDO('sqlite:database.db');

        $pdo->exec('SET NAMES utf8');

        $pdo->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
    $stmt = $pdo->prepare("SELECT * FROM users WHERE email = ?");
    $stmt->execute([$iden]);
    $row = $stmt->fetch();
      
    if ($row && password_verify($mdp, $row['password'])) {
        header('Location: index.php');
        $_SESSION['connected'] = true;
        $_SESSION['user'] = $row['name'];
    }else{
        header('Location: Login.php');
        
        
    }
}
